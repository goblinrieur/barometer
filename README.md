# Barometer

barometer in addition to existing project [WSKS](https://gitlab.com/goblinrieur/WSKS)

For this one it can be standalone or build in the same 3D printed box than WSKW.

LICENSE is [MIT](./LICENSE)

# Datasheets

[LM324](./Datasheets/LM324.pdf)

[LM3914](./Datasheets/lm3914.pdf)

[MPX2202](./Datasheets/MPX2202-1127021.pdf)

[THM10-0521](./Datasheets/THM10-0521.pdf)

[cd4001](./Datasheets/cd4001ub.pdf)

# Schema

![Schema](./pics/schema_BW.png)

# Pictures

![barometerpcbcopper](./pics/barometerpcbcopper.png)

![PCB](./pics/barometerpcbup.png)

# Production files

[Gerber](./Production/)

# How to

MPX2200AP sensor gets 0 to 2000 hPa values, with a resolution of 1mv/hPa. It is powered by a +5volts/-5volts symmetric voltages. Its outputs S+/S- are not related to ground.

This results on a very low voltage value, so LM324 4Th amplifier is used. To display the pressure I use a bar-graph management with the LM3914. The 4001 is used to display the atmospheric pressure trend on three LED's.

Yellow led means pressure is stable. The green one means pressure is rising up. The red one mean pressure is getting lower.
